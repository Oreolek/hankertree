power = () ->
  if salet.character.weapons > salet.character.people
    return salet.character.people
  return Math.floor(salet.character.weapons + (salet.character.people - salet.character.weapons) / 2)

salet.init = () ->
  @character.people = 10 # количество людей в деревне (сражаются и дети, и женщины)
  @character.food = 16 # количество еды в деревне
  @character.weapons = 3 # количество оружия в деревне
  @character.dice = 4 # кубик шансов - можно улучшать
  @character.update_sidebar = () ->
    jQuery(".sidebar").html(marked("sidebar".l({
      "people": @people,
      "food": @food,
      "weapons": @weapons,
      "dice": @dice,
      "power": power(),
      "help": makebtn("help_btn".l(), "help")
    })))

  salet.improv = new Improv(spec, {
    filters: [Improv.filters.mismatchFilter()]
  })

room "start",
  dsc: () -> "start".l()

sysroom "help",
  dsc: () -> "help".l()

croom "home",
  dsc: () ->
    "home".l({
      "people": salet.character.people,
      "food": salet.character.food,
      "weapons": salet.character.weapons,
      "dice": salet.character.dice
    })
  choices: "#attack"

croom "attack",
  village: ""
  people: 0
  tags: ["attack"]
  optionText: () ->
    @village = salet.rnd.randomElement("villages".l())
    @people = salet.rnd.randRange(7, 20)
    power = power()
    chance = salet.rnd.odds(power, @people, salet.character.dice)
    return "attack".l()+" #{@village} (#{@people} "+"people".l()+" - #{chance}% "+"to_win".l()+")"
  dsc: () ->
    "Вы захватываете #{@village}."
