salet.game_id = "2156049f-ca32-4460-a363-feb1caaa2a39"
salet.game_version = "1.0"

$.holdReady( true )
$.getJSON('game/translations/'+i18n.lang+'.json', (data) ->
  i18n.push(i18n.lang, data)

  $.holdReady( false )
)

$(document).ready(() ->
  window.addEventListener('popstate', (event) ->
    salet.goBack()
  )
  $("#night").on("click", () ->
    if (window.night)
      styles = {
        "-webkit-filter": ""
        "filter": ""
        "background-color": ""
      }
      $("body").css(styles)
      $("#night").removeClass("active")
      window.night = false
    else
      styles = {
        "-webkit-filter": "invert(1)hue-rotate(180deg)"
        "filter": "invert(1)hue-rotate(180deg)"
        "background-color": "#000"
      }
      $("body").css(styles)
      $("#night").addClass("active")
      window.night = true
  )

  salet.beginGame()
)

makebtn = (title, anchor) ->
  """
    <a href="#{anchor}"><button class="btn btn-lg btn-outline-primary">#{title}</button></a>
  """

croom = (name, spec) ->
  spec.clear = false
  spec.optionText ?= """
    <div class="#{spec.optionColor}">
      <div class="title">#{spec.title}</div>
      <div class="subtitle">#{spec.subtitle}</div>
    </div>
  """
  spec.enter = () ->
    salet.character.update_sidebar()
    if @onEnter?
      @onEnter()
  return room(name, spec)

sysroom = (name, spec) ->
  spec.canSave = false
  spec.enter = () ->
    $(".sidebar").hide()
  spec.exit = () ->
    $(".sidebar").show()
  spec.after = () -> """
    <div class="center">#{makebtn("back".l(), "./exit")}</div>
    """
  spec.actions =
    exit: () ->
      return salet.goBack()
  return room(name, spec)

spec = {
  animal: {
    groups: [
      {
        tags: [['class', 'mammal']],
        phrases: ['dog', 'cat']
      },
      {
        tags: [['class', 'bird']],
        phrases: ['parrot']
      }
    ]
  },
  root: {
    groups: [
      {
        tags: [],
        phrases: [
          "[name]: I have a [:animal] who is [#2-7] years old."
        ]
      }
    ]
  }
}
